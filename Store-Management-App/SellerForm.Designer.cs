﻿
namespace Store_Management_App
{
    partial class SellerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SellerForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.Sphone = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Spass = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Sname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Sage = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Sid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.SellerDGV = new Guna.UI.WinForms.GunaDataGridView();
            this.materialButton4 = new MaterialSkin.Controls.MaterialButton();
            this.materialButton6 = new MaterialSkin.Controls.MaterialButton();
            this.Addbutton = new MaterialSkin.Controls.MaterialButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.materialButton5 = new MaterialSkin.Controls.MaterialButton();
            this.materialButton3 = new MaterialSkin.Controls.MaterialButton();
            this.materialButton2 = new MaterialSkin.Controls.MaterialButton();
            this.materialButton1 = new MaterialSkin.Controls.MaterialButton();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SellerDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.Controls.Add(this.Sphone);
            this.panel1.Controls.Add(this.Spass);
            this.panel1.Controls.Add(this.Sname);
            this.panel1.Controls.Add(this.Sage);
            this.panel1.Controls.Add(this.Sid);
            this.panel1.Controls.Add(this.SellerDGV);
            this.panel1.Controls.Add(this.materialButton4);
            this.panel1.Controls.Add(this.materialButton6);
            this.panel1.Controls.Add(this.Addbutton);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.ForeColor = System.Drawing.Color.Chocolate;
            this.panel1.Location = new System.Drawing.Point(130, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(734, 506);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Sphone
            // 
            this.Sphone.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Sphone.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Sphone.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Sphone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Sphone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Sphone.ForeColor = System.Drawing.Color.Black;
            this.Sphone.HintForeColor = System.Drawing.Color.Empty;
            this.Sphone.HintText = "";
            this.Sphone.isPassword = false;
            this.Sphone.LineFocusedColor = System.Drawing.Color.Blue;
            this.Sphone.LineIdleColor = System.Drawing.SystemColors.HotTrack;
            this.Sphone.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.Sphone.LineThickness = 3;
            this.Sphone.Location = new System.Drawing.Point(105, 272);
            this.Sphone.Margin = new System.Windows.Forms.Padding(4);
            this.Sphone.MaxLength = 32767;
            this.Sphone.Name = "Sphone";
            this.Sphone.Size = new System.Drawing.Size(184, 33);
            this.Sphone.TabIndex = 20;
            this.Sphone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // Spass
            // 
            this.Spass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Spass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Spass.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Spass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Spass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Spass.ForeColor = System.Drawing.Color.Black;
            this.Spass.HintForeColor = System.Drawing.Color.Empty;
            this.Spass.HintText = "";
            this.Spass.isPassword = false;
            this.Spass.LineFocusedColor = System.Drawing.Color.Blue;
            this.Spass.LineIdleColor = System.Drawing.SystemColors.HotTrack;
            this.Spass.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.Spass.LineThickness = 3;
            this.Spass.Location = new System.Drawing.Point(105, 333);
            this.Spass.Margin = new System.Windows.Forms.Padding(4);
            this.Spass.MaxLength = 32767;
            this.Spass.Name = "Spass";
            this.Spass.Size = new System.Drawing.Size(184, 33);
            this.Spass.TabIndex = 19;
            this.Spass.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // Sname
            // 
            this.Sname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Sname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Sname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Sname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Sname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Sname.ForeColor = System.Drawing.Color.Black;
            this.Sname.HintForeColor = System.Drawing.Color.Empty;
            this.Sname.HintText = "";
            this.Sname.isPassword = false;
            this.Sname.LineFocusedColor = System.Drawing.Color.Blue;
            this.Sname.LineIdleColor = System.Drawing.SystemColors.HotTrack;
            this.Sname.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.Sname.LineThickness = 3;
            this.Sname.Location = new System.Drawing.Point(105, 162);
            this.Sname.Margin = new System.Windows.Forms.Padding(4);
            this.Sname.MaxLength = 32767;
            this.Sname.Name = "Sname";
            this.Sname.Size = new System.Drawing.Size(184, 33);
            this.Sname.TabIndex = 18;
            this.Sname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // Sage
            // 
            this.Sage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Sage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Sage.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Sage.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Sage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Sage.ForeColor = System.Drawing.Color.Black;
            this.Sage.HintForeColor = System.Drawing.Color.Empty;
            this.Sage.HintText = "";
            this.Sage.isPassword = false;
            this.Sage.LineFocusedColor = System.Drawing.Color.Blue;
            this.Sage.LineIdleColor = System.Drawing.SystemColors.HotTrack;
            this.Sage.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.Sage.LineThickness = 3;
            this.Sage.Location = new System.Drawing.Point(105, 214);
            this.Sage.Margin = new System.Windows.Forms.Padding(4);
            this.Sage.MaxLength = 32767;
            this.Sage.Name = "Sage";
            this.Sage.Size = new System.Drawing.Size(184, 33);
            this.Sage.TabIndex = 17;
            this.Sage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // Sid
            // 
            this.Sid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Sid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Sid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Sid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Sid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Sid.ForeColor = System.Drawing.Color.Black;
            this.Sid.HintForeColor = System.Drawing.Color.Empty;
            this.Sid.HintText = "";
            this.Sid.isPassword = false;
            this.Sid.LineFocusedColor = System.Drawing.Color.Blue;
            this.Sid.LineIdleColor = System.Drawing.SystemColors.HotTrack;
            this.Sid.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.Sid.LineThickness = 3;
            this.Sid.Location = new System.Drawing.Point(105, 111);
            this.Sid.Margin = new System.Windows.Forms.Padding(4);
            this.Sid.MaxLength = 32767;
            this.Sid.Name = "Sid";
            this.Sid.Size = new System.Drawing.Size(184, 33);
            this.Sid.TabIndex = 14;
            this.Sid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // SellerDGV
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.SellerDGV.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.SellerDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SellerDGV.BackgroundColor = System.Drawing.Color.White;
            this.SellerDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SellerDGV.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.SellerDGV.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SellerDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.SellerDGV.ColumnHeadersHeight = 4;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Chocolate;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SellerDGV.DefaultCellStyle = dataGridViewCellStyle3;
            this.SellerDGV.EnableHeadersVisualStyles = false;
            this.SellerDGV.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.SellerDGV.Location = new System.Drawing.Point(296, 73);
            this.SellerDGV.Name = "SellerDGV";
            this.SellerDGV.RowHeadersVisible = false;
            this.SellerDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SellerDGV.Size = new System.Drawing.Size(434, 454);
            this.SellerDGV.TabIndex = 11;
            this.SellerDGV.Theme = Guna.UI.WinForms.GunaDataGridViewPresetThemes.Guna;
            this.SellerDGV.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.SellerDGV.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.SellerDGV.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.SellerDGV.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.SellerDGV.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.SellerDGV.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.SellerDGV.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.SellerDGV.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.SellerDGV.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.SellerDGV.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.SellerDGV.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.SellerDGV.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.SellerDGV.ThemeStyle.HeaderStyle.Height = 4;
            this.SellerDGV.ThemeStyle.ReadOnly = false;
            this.SellerDGV.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.SellerDGV.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.SellerDGV.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.SellerDGV.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.Chocolate;
            this.SellerDGV.ThemeStyle.RowsStyle.Height = 22;
            this.SellerDGV.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.SellerDGV.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.SellerDGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SellerDGV_CellContentClick);
            // 
            // materialButton4
            // 
            this.materialButton4.AutoSize = false;
            this.materialButton4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton4.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButton4.Depth = 0;
            this.materialButton4.HighEmphasis = true;
            this.materialButton4.Icon = null;
            this.materialButton4.Location = new System.Drawing.Point(199, 386);
            this.materialButton4.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton4.Name = "materialButton4";
            this.materialButton4.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButton4.Size = new System.Drawing.Size(73, 36);
            this.materialButton4.TabIndex = 5;
            this.materialButton4.Text = "Delete";
            this.materialButton4.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton4.UseAccentColor = false;
            this.materialButton4.UseVisualStyleBackColor = true;
            this.materialButton4.Click += new System.EventHandler(this.materialButton4_Click);
            // 
            // materialButton6
            // 
            this.materialButton6.AutoSize = false;
            this.materialButton6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton6.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButton6.Depth = 0;
            this.materialButton6.HighEmphasis = true;
            this.materialButton6.Icon = null;
            this.materialButton6.Location = new System.Drawing.Point(105, 386);
            this.materialButton6.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton6.Name = "materialButton6";
            this.materialButton6.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButton6.Size = new System.Drawing.Size(66, 36);
            this.materialButton6.TabIndex = 6;
            this.materialButton6.Text = "edit";
            this.materialButton6.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton6.UseAccentColor = false;
            this.materialButton6.UseVisualStyleBackColor = true;
            this.materialButton6.Click += new System.EventHandler(this.materialButton6_Click);
            // 
            // Addbutton
            // 
            this.Addbutton.AutoSize = false;
            this.Addbutton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Addbutton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.Addbutton.Depth = 0;
            this.Addbutton.HighEmphasis = true;
            this.Addbutton.Icon = null;
            this.Addbutton.Location = new System.Drawing.Point(20, 386);
            this.Addbutton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Addbutton.MouseState = MaterialSkin.MouseState.HOVER;
            this.Addbutton.Name = "Addbutton";
            this.Addbutton.NoAccentTextColor = System.Drawing.Color.Empty;
            this.Addbutton.Size = new System.Drawing.Size(56, 36);
            this.Addbutton.TabIndex = 7;
            this.Addbutton.Text = "ADD";
            this.Addbutton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.Addbutton.UseAccentColor = false;
            this.Addbutton.UseVisualStyleBackColor = true;
            this.Addbutton.Click += new System.EventHandler(this.Addbutton_Click);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(-5, 333);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 38);
            this.label6.TabIndex = 9;
            this.label6.Text = "Password";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(-3, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 38);
            this.label5.TabIndex = 7;
            this.label5.Text = "Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(-5, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 38);
            this.label4.TabIndex = 5;
            this.label4.Text = "Age";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(-3, 272);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 38);
            this.label3.TabIndex = 3;
            this.label3.Text = "Phone no.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(-5, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 38);
            this.label2.TabIndex = 1;
            this.label2.Text = "ID";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(193, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(326, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "Manage Sellers";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // materialButton5
            // 
            this.materialButton5.AutoSize = false;
            this.materialButton5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton5.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButton5.Depth = 0;
            this.materialButton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialButton5.HighEmphasis = true;
            this.materialButton5.Icon = null;
            this.materialButton5.Location = new System.Drawing.Point(836, 4);
            this.materialButton5.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton5.Name = "materialButton5";
            this.materialButton5.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButton5.Size = new System.Drawing.Size(31, 27);
            this.materialButton5.TabIndex = 5;
            this.materialButton5.Text = "X";
            this.materialButton5.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton5.UseAccentColor = false;
            this.materialButton5.UseVisualStyleBackColor = true;
            this.materialButton5.Click += new System.EventHandler(this.materialButton5_Click);
            // 
            // materialButton3
            // 
            this.materialButton3.AutoSize = false;
            this.materialButton3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton3.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButton3.Depth = 0;
            this.materialButton3.HighEmphasis = true;
            this.materialButton3.Icon = null;
            this.materialButton3.Location = new System.Drawing.Point(13, 133);
            this.materialButton3.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton3.Name = "materialButton3";
            this.materialButton3.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButton3.Size = new System.Drawing.Size(104, 36);
            this.materialButton3.TabIndex = 8;
            this.materialButton3.Text = "Category";
            this.materialButton3.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton3.UseAccentColor = false;
            this.materialButton3.UseVisualStyleBackColor = true;
            // 
            // materialButton2
            // 
            this.materialButton2.AutoSize = false;
            this.materialButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton2.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButton2.Depth = 0;
            this.materialButton2.HighEmphasis = true;
            this.materialButton2.Icon = null;
            this.materialButton2.Location = new System.Drawing.Point(13, 203);
            this.materialButton2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton2.Name = "materialButton2";
            this.materialButton2.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButton2.Size = new System.Drawing.Size(104, 36);
            this.materialButton2.TabIndex = 7;
            this.materialButton2.Text = "Selling";
            this.materialButton2.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton2.UseAccentColor = false;
            this.materialButton2.UseVisualStyleBackColor = true;
            // 
            // materialButton1
            // 
            this.materialButton1.AutoSize = false;
            this.materialButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton1.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.materialButton1.Depth = 0;
            this.materialButton1.HighEmphasis = true;
            this.materialButton1.Icon = null;
            this.materialButton1.Location = new System.Drawing.Point(13, 62);
            this.materialButton1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton1.Name = "materialButton1";
            this.materialButton1.NoAccentTextColor = System.Drawing.Color.Empty;
            this.materialButton1.Size = new System.Drawing.Size(104, 36);
            this.materialButton1.TabIndex = 6;
            this.materialButton1.Text = "Seller";
            this.materialButton1.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton1.UseAccentColor = false;
            this.materialButton1.UseVisualStyleBackColor = true;
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // SellerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 554);
            this.Controls.Add(this.materialButton3);
            this.Controls.Add(this.materialButton2);
            this.Controls.Add(this.materialButton1);
            this.Controls.Add(this.materialButton5);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SellerForm";
            this.Text = "SellerForm";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SellerDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Sname;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Sage;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Sid;
        private Guna.UI.WinForms.GunaDataGridView SellerDGV;
        private MaterialSkin.Controls.MaterialButton materialButton4;
        private MaterialSkin.Controls.MaterialButton materialButton6;
        private MaterialSkin.Controls.MaterialButton Addbutton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialButton materialButton5;
        private MaterialSkin.Controls.MaterialButton materialButton3;
        private MaterialSkin.Controls.MaterialButton materialButton2;
        private MaterialSkin.Controls.MaterialButton materialButton1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Sphone;
        private Bunifu.Framework.UI.BunifuMaterialTextbox Spass;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
    }
}