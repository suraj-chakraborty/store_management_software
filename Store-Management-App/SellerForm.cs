﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Store_Management_App
{
    public partial class SellerForm : Form
    {
        public SellerForm()
        {
            InitializeComponent();
        }
        private void populate()
        {
            con.Open();
            string query = "select * from SellerTbl";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            SellerDGV.DataSource = ds.Tables[0];
            con.Close();
        }


        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\suraj\Documents\smarketdb.mdf;Integrated Security=True;Connect Timeout=30");
        private void Addbutton_Click(object sender, EventArgs e)
            {
                try
                {
                    con.Open();
                    string query = "insert into SellerTbl values(" + Sid.Text + ",'" + Sname.Text + "','" + Sage.Text + "','" + Sphone.Text +"','" + Spass.Text +"')";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("category added Successfully");
                    con.Close();
                    populate();
                    Sid.Text = "";
                    Sname.Text = "";
                    Sphone.Text = "";
                    Spass.Text = "";
                    Sage.Text = ""; 

            }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        

        private void SellerDGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
              Sid.Text = SellerDGV.SelectedRows[0].Cells[0].Value.ToString();
              Sname.Text = SellerDGV.SelectedRows[0].Cells[1].Value.ToString();
              Sage.Text = SellerDGV.SelectedRows[0].Cells[2].Value.ToString();
              Sphone.Text = SellerDGV.SelectedRows[0].Cells[3].Value.ToString();
              Spass.Text = SellerDGV.SelectedRows[0].Cells[4].Value.ToString();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            populate();
        }

        private void materialButton5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void materialButton6_Click(object sender, EventArgs e)
        {
            try
            {
                if (Sid.Text == "" || Sname.Text == "" || Sage.Text == "" || Sphone.Text == "" || Spass.Text == "")
                {
                    MessageBox.Show("cell is Empty");
                }
                else
                {
                    con.Open();
                    string query = "update SellerTbl set SellerName='" + Sname.Text + "',SellerAge='" + Sage.Text + "',SellerPhone='" + Sphone.Text + "',SellerPass='" +Spass.Text+ "' where Sellerid="+Sid.Text+";";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Category successfully Updated");
                    con.Close();
                    populate();
                };
            }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void materialButton4_Click(object sender, EventArgs e)
        {
            try
            {
                if (Sid.Text == "")
                {
                    MessageBox.Show("Select a Product to delete");
                }
                else
                {
                    con.Open();
                    string query = "delete from SellerTbl where Sellerid="+Sid.Text +"";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Seller deleted Successfully");
                    con.Close();
                    populate();
                    Sid.Text = "";
                    Sname.Text = "";
                    Sphone.Text = "";
                    Spass.Text = "";
                    Sage.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
    }
}
